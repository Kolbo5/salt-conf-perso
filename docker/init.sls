docker_repo:
  pkgrepo.managed:
    - name: deb https://apt.dockerproject.org/repo ubuntu-xenial main
    - file: /etc/apt/sources.list.d/docker.list
    - keyserver: p80.pool.sks-keyservers.net
    - keyid: 2C52609D
    - require_in:
      - docker_install
      - docker_pkg-requirments

docker_install:
  pkg.latest:
    - pkgs:
      - docker-engine
    - require:
      - docker_pkg-requirments

docker_pkg-requirments:
  pkg.latest:
    - pkgs:
      - linux-image-generic 
      - linux-image-extra-virtual