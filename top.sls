base: #nom de conf par défaut
  '*': #toutes les machines
    - system
  'desktop':
    - browsers.firefox
    - chat.telegram
    - chat.signal
    - chat.pidgin
    - keepass2
    - media.nextcloud
    - media.spotify
    - media.vlc
    - media.youtube-dl
    - snapd
    - users
    - veracrypt
    - filezilla
  'dev':
    - dev.python
    - dev.npm
    - editors.meld
    - editors.vim
    - editors.vscode # TODO : regarder si faut pascouper en deux
    - network
    - shell
    - version-control
  'dev-boulot':
    - browsers.chromium
    - chat.slack
    - dev.python356
    - docker
    - virtualbox
  'autres':
    - containers # Oodnadatta
    - plex
    

