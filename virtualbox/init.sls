virtualbox_repo:
  pkgrepo.managed:
    - name: deb https://download.virtualbox.org/virtualbox/debian bionic main
    - file: /etc/apt/sources.list.d/virtualbox.list
    - key_url: https://www.virtualbox.org/download/oracle_vbox_2016.asc
    - require_in:
      - virtualbox_install

virtualbox_install:
  pkg.latest:
    - pkgs:
      - virtualbox
      - virtualbox-dkms
      - virtualbox-qt
      - virtualbox-ext-pack
      - virtualbox-guest-utils
