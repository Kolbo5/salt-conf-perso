python356-folder:
  archive:
    - extracted
    - name: /tmp/
    - source: https://www.python.org/ftp/python/3.5.6/Python-3.5.6rc1.tgz
    - source_hash: md5=66d1ab013fa6669593f77dcb0ea93003
    - archive_format: tar
    - options: z
    - if_missing: /tmp/Python-3.5.6rc1

install-python356:
  cmd.run:
    - name: |
        cd /tmp/Python-3.5.6rc1
        ./configure
        make altinstall
    - cwd: /tmp
    - shell: /bin/bash
    - timeout: 300
    - unless: test -x /usr/local/bin/python3.5
    - require:
      - python356-folder
