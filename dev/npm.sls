nodejs_repo:
  pkgrepo.managed:
    - name: deb https://deb.nodesource.com/node_6.x bionic main
    - file: /etc/apt/sources.list.d/nodejs.list
    - key_url: https://deb.nodesource.com/gpgkey/nodesource.gpg.key
    - require_in:
      - nodejs_install

nodejs_install:
  pkg.latest:
    - pkgs:
      - nodejs
      - npm
      - build-essential
      - make

update_npm:
  cmd.run:
    - name: npm install -g npm
    - user: root
    - require:
      - nodejs_install

yarn_repo:
  pkgrepo.managed:
    - name: deb https://dl.yarnpkg.com/debian/ stable main
    - file: /etc/apt/sources.list.d/yarn.list
    - key_url: https://dl.yarnpkg.com/debian/pubkey.gpg
    - require_in:
      - yarn_install

yarn_install:
  pkg.latest:
    - pkgs:
      - yarn