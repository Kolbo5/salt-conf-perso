python-packages:
  pkg.installed:
    - pkgs:
      - python3.6
      - python3-venv
      - python3-dev
      - ipython3
      - python3-pip
      - python3-setuptools
