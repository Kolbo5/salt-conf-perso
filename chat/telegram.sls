{% from 'macros.sls' import dotfile %}

snap_telegram-desktop:
  cmd.run:
    - name: snap install telegram-desktop
    - unless: snap list telegram-desktop
    - output_loglevel: quiet
    - require:
      - snapd_service

{{ dotfile('.config/autostart/telegram-desktop_telegramdesktop.desktop', 'dotfiles/telegram/telegram-desktop_telegramdesktop.desktop', 'snap_telegram-desktop') }}