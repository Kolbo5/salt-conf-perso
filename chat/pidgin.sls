{% from 'macros.sls' import dotfile %}

pidgin_repo:
  pkgrepo.managed:
    - name: deb http://ppa.launchpad.net/sipe-collab/ppa/ubuntu bionic main
    - file: /etc/apt/sources.list.d/pidgin.list
    - keyserver: keyserver.ubuntu.com
    - keyid: F93FF666
    - require_in:
      - pidgin_install

pidgin_install:
  pkg.latest:
    - pkgs:
      - pidgin
      - pidgin-sipe
      - pidgin-extprefs
      - gnome-panel

{{ dotfile('.purple/prefs.xml', 'dotfiles/pidgin/prefs.xml', 'pidgin_install') }}
{{ dotfile('.purple/accounts.xml', 'dotfiles/pidgin/accounts.xml', 'pidgin_install') }}
{{ dotfile('.config/autostart/pidgin.desktop', 'dotfiles/pidgin/pidgin.desktop', 'pidgin_install') }}
