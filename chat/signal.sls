{% from 'macros.sls' import dotfile %}

signal-desktop_repo:
  pkgrepo.managed:
    - name: deb https://updates.signal.org/desktop/apt xenial main
    - file: /etc/apt/sources.list.d/signal.list
    - key_url: https://updates.signal.org/desktop/apt/keys.asc
    - require_in:
      - signal-desktop_install

signal-desktop_install:
  pkg.latest:
    - pkgs:
      - signal-desktop

{{ dotfile('.config/autostart/signal.desktop', 'dotfiles/signal/signal.desktop', 'signal-desktop_install') }}
