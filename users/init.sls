eolivier:
  user.present:
    - fullname: Kolbo V
    - shell: /bin/zsh
    - home: /home/eolivier
    - uid: 1000
    - gid: 1000

sudo:
  pkg.installed: []
  file.managed:
    - name: /etc/sudoers.d/eolivier
    - user: root
    - group: root
    - mode: 440
    - contents:
      - 'eolivier ALL=(ALL) NOPASSWD:ALL'
    - require:
      - pkg: sudo
