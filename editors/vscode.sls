{% from 'macros.sls' import dotfile %}

vscode_repo:
  pkgrepo.managed:
    - name: deb https://packages.microsoft.com/repos/vscode stable main
    - file: /etc/apt/sources.list.d/microsoft-vscode.list
    - key_url: salt://editors/files/microsoft-vscode.gpg
    - require_in:
      - vscode_install

vscode_install:
  pkg.latest:
    - pkgs:
      - code
      - fonts-firacode


{# 
Il faudrait gérer l'install depuis un fichier contenant la liste des extension dans vscode...
code --list-extensions
    List the installed extensions.
code --install-extension ( | )
    Installs an extension. 

En attendant, à la main : 
Sauvegarder les extensions installées : 
code --list-extensions > .vscode/extensions

Installer les extensions listées dans le fichier extensions :
while read in; do code --install-extension "$in"; done < .vscode/extensions_list
#}

vscode_install_extensions:
  cmd.run:
    - name: while read in; do code --install-extension "$in" --force; done < .vscode/extensions_list
    - runas: eolivier
    - require:
      - vscode_install

{{ dotfile('.config/Code/User/settings.json', 'dotfiles/vscode/settings.json', 'vscode_install') }}
{{ dotfile('.config/Code/User/keybindings.json', 'dotfiles/vscode/keybindings.json', 'vscode_install') }}
{{ dotfile('.vscode/extensions_list', 'dotfiles/vscode/extensions_list', 'vscode_install') }}
{{ dotfile('src/.workspaces', 'dotfiles/vscode/workspaces', 'vscode_install') }}
{{ dotfile('.editorconfig', 'dotfiles/editorconfig') }}
