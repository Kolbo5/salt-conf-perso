{% from 'macros.sls' import dotfile %}

git:
  pkg.installed

{{ dotfile('.gitconfig', 'dotfiles/gitconfig', 'git') }}
