# salt-conf-perso : configuration SaltStack personnelle

La configuration de SaltStack se trouve dans `/srv/salt`

## Pour installer ma configuration Salt sur Ubuntu
1) Télécharger et installer le paquet (on en profite pour install git).
```sh
sudo apt install salt-minion git
```

2) Créer le répertoire pour salt.
```sh
sudo mkdir -p /srv
```

3) Récupérer tous les fichiers :

3.1) mettre la conf salt dans `/srv/salt`.
```sh
sudo mkdir /srv/salt
cd /srv/salt
sudo git clone https://framagit.org/Kolbo5/salt-conf-perso.git .
```

3.2) et les fichiers de conf dans `/srv/salt-files`
```sh
sudo mkdir /srv/salt-files
cd /srv/salt-files
sudo git clone https://framagit.org/Kolbo5/dotfiles.git .
```

4) Lancer salt et appliquer la configuration saltstack sur la machine.
```
sudo apt update #(met à jour la liste des paquets disponibles à l'installation)
sudo salt-call --local state.highstate test=True
sudo salt-call --local state.highstate
sudo apt ugrade #(met à jour tous les paquets sur le système pour qu'ils soient tous à la dernière version)
```

## Pour ajouter de nouveaux programmes à la configuration
Chaque fichier .sls dans `/srv/salt` contient des règles pour salt. Ils sont pris en compte uniquement s'ils sont listés dans le fichier `top.sls`.
Il peut également s'agir de fichiers `init.sls` dans un répertoire.

1) Ajouter le logiciel au fichier `nom_du_fichier.sls`

2) Lancer salt-call pour installer le nouveau programme
```
sudo salt-call --local state.sls nom_du_fichier
```

## Reste à faire :
- découper en "pour un PC perso avec IHM" et "pour un serv" (pour l'instant tout est en vrac, desktop + serv s'installent...) ;
- créer un fichier `run.sh` qui automatise l'installation du minion, le téléchargement des recettes salt, etc. en s'inspirant de [celui de mrwilson](https://github.com/mrwilson/salt-config/blob/master/run.sh) par exemple... ;)
- mettre le user (eolivier) en argument quelque plutot qu'en dur dans plusieurs fichiers...
- utiliser les pillard pour le nom de l'user et aussi pour les symlinc dans le home.

## crédits
Repo inspiré de ceux d'[Oodnadatta](https://github.com/Oodnadatta/salt-conf-perso) et de [mrwilson](https://github.com/mrwilson/salt-config).
Plex formula récupérée chez [saltstack-formulas](https://github.com/saltstack-formulas/plex-formula).