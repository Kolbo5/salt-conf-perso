{% from 'macros.sls' import dotfile %}

veracrypt_repo:
  pkgrepo.managed:
    - name: deb http://ppa.launchpad.net/unit193/encryption/ubuntu bionic main
    - file: /etc/apt/sources.list.d/veracrypt.list
    - keyserver: keyserver.ubuntu.com
    - keyid: B58A653A
    - require_in:
      - veracrypt

veracrypt:
  pkg.latest:
    - pkgs:
      - veracrypt
      
{{ dotfile('.config/VeraCrypt/Configuration.xml', 'dotfiles/veracrypt/Configuration.xml', 'veracrypt') }}
{{ dotfile('.config/autostart/veracrypt.desktop', 'dotfiles/veracrypt/veracrypt.desktop', 'veracrypt') }}
