{% from 'macros.sls' import dotfile %}

network.pkgs:
  pkg.installed:
    - pkgs:
      - curl
      - openssh-client


{{ dotfile('.ssh/config', 'dotfiles/ssh/config', 'network.pkgs') }}
