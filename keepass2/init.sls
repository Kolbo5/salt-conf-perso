{% from 'macros.sls' import dotfile %}

keepass2:
  pkg.latest:
    - pkgs:
      - keepass2
      - mono-complete

keepass2-plugin_repo:
  pkgrepo.managed:
    - name: deb http://ppa.launchpad.net/dlech/keepass2-plugins/ubuntu bionic main
    - file: /etc/apt/sources.list.d/keepass2.list
    - keyserver: keyserver.ubuntu.com
    - keyid: CC10C662
    - require_in:
      - keepass2-plugins

keepass2-plugins:
  pkg.latest:
    - pkgs:
      - keepass2-plugin-tray-icon
      - keepass2-plugin-keeagent
      - keepass2-plugin-rpc # firefox link
      - xul-ext-keebird # thunderbird link
    - require:
      - keepass2

Keepass Keybindings | Ficheir de conf des raccourcs à importer dans les raccourcis système:
  file.managed:
    - name: /home/eolivier/.config/KeePass/keepass_keybindings.dconf
    - source: /srv/salt-files/dotfiles/keepass2/keepass_keybindings.dconf
    - user: eolivier
    - group: eolivier
    - mode: 644

Keepass Keybindings | script à lancer pour importer les raccourcis du fichier de conf:
  file.managed:
    - name: /home/eolivier/.config/KeePass/init_keepass_keybindings.sh
    - source: /srv/salt-files/dotfiles/keepass2/init_keepass_keybindings.sh
    - user: eolivier
    - group: eolivier
    - mode: 755

Keepass Keybindings | .descktop d'auto-start, c'est lui qui lancera le script d'import à chaque demmarage:
  file.managed:
    - name: /home/eolivier/.config/autostart/keepass_keybindings.desktop
    - source: /srv/salt-files/dotfiles/keepass2/keepass_keybindings.desktop
    - user: eolivier
    - group: eolivier
    - mode: 644

Keepass Keybindings | script nécessaire pour le raccourci de vérouillage:
  file.managed:
    - name: /home/eolivier/.config/KeePass/keepass_lock_when_screen_saver.sh
    - source: /srv/salt-files/dotfiles/keepass2/keepass_lock_when_screen_saver.sh
    - user: eolivier
    - group: eolivier
    - mode: 755

# config de keepass
{{ dotfile('.config/KeePass/KeePass.config.xml', 'dotfiles/keepass2/KeePass.config.xml', 'keepass2') }}
# lancement auto de keepass
{{ dotfile('.config/autostart/keepass2.desktop', 'dotfiles/keepass2/keepass2.desktop', 'keepass2') }} 
