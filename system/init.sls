system.pkgs:
  pkg.installed:
    - pkgs:
      - dfc #permet de connaître l'espace restant sur les périphériques de stockage
      - htop
      - ncdu #comme WinDirStat
      - tree
      - ccze # permet de lire les logs en couleurs
