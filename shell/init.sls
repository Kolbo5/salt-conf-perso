{% from 'macros.sls' import dotfile %}

zsh:
  pkg.installed

p7zip-full:
  pkg.installed

tmux:
  pkg.installed

change_shell:
    module.run:
        - name: user.chshell
        - m_name: eolivier
        - shell: /usr/bin/zsh
        - require:
            - pkg: zsh

https://github.com/robbyrussell/oh-my-zsh.git:
    git.latest:
        - rev: master
        - target: "/home/eolivier/.oh-my-zsh"
        - unless: "test -d /home/eolivier/.oh-my-zsh"
        - require:
            - pkg: git
            - pkg: zsh

{{ dotfile('.zshrc', 'dotfiles/zshrc', 'zsh') }}
{{ dotfile('.tmux.conf', 'dotfiles/tmux/tmux.conf', 'tmux') }}
{{ dotfile('.tmux/session-dev.init', 'dotfiles/tmux/session-dev.init', 'tmux') }}
