{% from 'macros.sls' import dotfile %}

nextcloud-client_repo:
  pkgrepo.managed:
    {# En Bionic on a la version 2.5.x qui ne fonctionne pas (cf https://github.com/nextcloud/desktop/issues/863 )
    - name: deb http://ppa.launchpad.net/nextcloud-devs/client/ubuntu bionic main 
    d'où le downgrade en artful (version 2.3.3) qui elle fonctionne.
    #}
    - name: deb http://ppa.launchpad.net/nextcloud-devs/client/ubuntu artful main
    - file: /etc/apt/sources.list.d/nextcloud.list
    - keyserver: keyserver.ubuntu.com
    - keyid: AD3DD469
    - require_in:
      - nextcloud-client_install

nextcloud-client_install:
  pkg.latest:
    - pkgs:
      - nextcloud-client

{{ dotfile('.config/autostart/Nextcloud.desktop', 'dotfiles/nextcloud/Nextcloud.desktop', 'nextcloud-client_install') }}

/home/eolivier/Notes:
  file.symlink:
    - target: /home/eolivier/Nextcloud/Notes
    - force: True
    - makedirs: True
    - user: eolivier
    - group: eolivier
    - mode: 644
    - require:
      - pkg: nextcloud-client_install

/home/eolivier/z_archives:
  file.symlink:
    - target: /home/eolivier/Nextcloud/Archives
    - force: True
    - makedirs: True
    - user: eolivier
    - group: eolivier
    - mode: 644
    - require:
      - pkg: nextcloud-client_install

/home/eolivier/z_backup:
  file.symlink:
    - target: /home/eolivier/Nextcloud/backup
    - force: True
    - makedirs: True
    - user: eolivier
    - group: eolivier
    - mode: 644
    - require:
      - pkg: nextcloud-client_install

/home/eolivier/DocsBoulot:
  file.symlink:
    - target: /home/eolivier/Nextcloud/DocsBoulot
    - force: True
    - makedirs: True
    - user: eolivier
    - group: eolivier
    - mode: 644
    - require:
      - pkg: nextcloud-client_install

/home/eolivier/Documents:
  file.symlink:
    - target: /home/eolivier/Nextcloud/Documents
    - force: True
    - makedirs: True
    - user: eolivier
    - group: eolivier
    - mode: 644
    - require:
      - pkg: nextcloud-client_install

/home/eolivier/Images:
  file.symlink:
    - target: /home/eolivier/Nextcloud/Images
    - force: True
    - makedirs: True
    - user: eolivier
    - group: eolivier
    - mode: 644
    - require:
      - pkg: nextcloud-client_install

/home/eolivier/Photos:
  file.symlink:
    - target: /home/eolivier/Nextcloud/Photos
    - force: True
    - makedirs: True
    - user: eolivier
    - group: eolivier
    - mode: 644
    - require:
      - pkg: nextcloud-client_install

duchesse_folder:
  file.directory:
    - name: /home/eolivier/Duchesse
    - user: eolivier
    - group: eolivier
    - mode: 755
    - makedirs: True

/home/eolivier/Duchesse/autre:
  file.symlink:
    - target: "/home/eolivier/Nextcloud/Duchesse autre"
    - force: True
    - makedirs: True
    - user: eolivier
    - group: eolivier
    - mode: 644
    - require:
      - file: duchesse_folder
      - pkg: nextcloud-client_install

/home/eolivier/Duchesse/bureau:
  file.symlink:
    - target: "/home/eolivier/Nextcloud/Duchesse\ bureau"
    - force: True
    - makedirs: True
    - user: eolivier
    - group: eolivier
    - mode: 644
    - require:
      - file: duchesse_folder
      - pkg: nextcloud-client_install

/home/eolivier/Duchesse/communication:
  file.symlink:
    - target: "/home/eolivier/Nextcloud/Duchesse\ communication"
    - force: True
    - makedirs: True
    - user: eolivier
    - group: eolivier
    - mode: 644
    - require:
      - file: duchesse_folder
      - pkg: nextcloud-client_install

/home/eolivier/Duchesse/trésorerie:
  file.symlink:
    - target: "/home/eolivier/Nextcloud/Duchesse\ trésorerie"
    - force: True
    - makedirs: True
    - user: eolivier
    - group: eolivier
    - mode: 644
    - require:
      - file: duchesse_folder
      - pkg: nextcloud-client_install
