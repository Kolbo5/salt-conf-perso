
Media | youtube-dl | repo:
  pkgrepo.managed:
    - name: deb http://ppa.launchpad.net/rvm/smplayer/ubuntu bionic main 
    - file: /etc/apt/sources.list.d/youtube-dl.list
    - keyserver: keyserver.ubuntu.com
    - keyid: E4A4F4F4
    - require_in:
      - Media | youtube-dl | pkg

Media | youtube-dl | pkg:
  pkg.latest:
    - pkgs:
      - youtube-dl
