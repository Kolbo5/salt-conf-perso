snap_spotify:
  cmd.run:
    - name: snap install spotify
    - unless: snap list spotify
    - output_loglevel: quiet
    - require:
      - snapd_service