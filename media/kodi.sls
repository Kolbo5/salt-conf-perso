
Media | kodi | repo:
  pkgrepo.managed:
    - name: deb http://ppa.launchpad.net/team-xbmc/ppa/ubuntu bionic main
    - file: /etc/apt/sources.list.d/kodi.list
    - keyserver: keyserver.ubuntu.com
    {# - key_url: https://download.opensuse.org/repositories/home:/selmf/xUbuntu_18.04/Release.key #}
    - keyid: 91E7EE5E
    - require_in:
      - Media | kodi | pkg

Media | kodi | pkg:
  pkg.latest:
    - pkgs:
      - kodi
