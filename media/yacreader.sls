
Media | YACreader | repo:
  pkgrepo.managed:
    - name: deb http://download.opensuse.org/repositories/home:/selmf/xUbuntu_18.04/ / 
    - file: /etc/apt/sources.list.d/yacreader.list
    {# - keyserver: keyserver.ubuntu.com #}
    - key_url: https://download.opensuse.org/repositories/home:/selmf/xUbuntu_18.04/Release.key
    {# - keyid: E4A4F4F4 #}
    - require_in:
      - Media | YACreader | pkg

Media | YACreader | pkg:
  pkg.latest:
    - pkgs:
      - yacreader
