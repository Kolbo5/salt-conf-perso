
Media | jitsi | repo:
  pkgrepo.managed:
    - name: deb https://download.jitsi.org stable/
    - file: /etc/apt/sources.list.d/jitsi.list
    - key_url: https://download.jitsi.org/jitsi-key.gpg.key
    {# - keyserver: keyserver.ubuntu.com
    - keyid: 91E7EE5E #}
    - require_in:
      - Media | jitsi | pkg

Media | jitsi | pkg:
  pkg.latest:
    - pkgs:
      - jitsi
