snapd_service:
  service.running:
    - name: snapd
    - enable: true
    - require:
      - snapd_install

snapd_install:
  pkg.latest:
    - pkgs:
      - snapd
