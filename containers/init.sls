{% from 'macros.sls' import dotfile %}

lxc:
  pkg.installed

{{ dotfile('.config/lxc/default.conf', 'dotfiles/containers/default.conf', 'lxc') }}
